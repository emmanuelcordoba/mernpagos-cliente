import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from './components/auth/Login';
import Registro from './components/auth/Registro';
import Home from './components/Home';
import CategoriaState from './context/categorias/categoriaState';
import PagoState from './context/pagos/pagoState';

function App() {
  return (
    <CategoriaState>
      <PagoState>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/registro" component={Registro} />
          </Switch>
        </BrowserRouter>
      </PagoState>
    </CategoriaState>
  );
}

export default App;
