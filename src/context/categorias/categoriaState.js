import React, { useReducer } from 'react';
import categoriaContext from './categoriaContext';
import categoriaReducer from './categoriaReducer';
import { 
    FORMULARIO_CATEGORIA,
    OBTENER_CATEGORIAS,
    AGREGAR_CATEGORIA,
    MOSTRAR_ERROR_NUEVA_CATEGORIA,
    CATEGORIA_ACTUAL,
    ELIMINAR_CATEGORIA,
} from '../../actiontypes';
import { v4 as uuidv4 } from 'uuid';

const CategoriaState = props => {

    const categorias = [
        { id: 1, nombre: 'Alquiler'},
        { id: 2, nombre: 'Supermercado'},
        { id: 3, nombre: 'Luz'},
        { id: 4, nombre: 'Agua'},
    ];

    // Valor inicial
    const initialState = {
        formcategoria: false,
        errorformulario: false,
        categorias: [],
        categoria: null
    };

    // Dispatch para ejecutar las acciones
    const [state, dispatch] = useReducer(categoriaReducer, initialState);

    // Acciones del CRUD
    
    // Mostrar formulario de Nueva Categoría
    const mostrarFormulario = () => {
        dispatch({
            type: FORMULARIO_CATEGORIA
        });
    }

    // Obtener las categorias
    const obtenerCategorias = () => {
        dispatch({
            type: OBTENER_CATEGORIAS,
            payload: categorias
        })
    }

    // Agregar nueva categoria
    const agregarCategoria = categoria => {
        categoria.id = uuidv4();
        dispatch({
            type: AGREGAR_CATEGORIA,
            payload: categoria
        });        
    }

    // Mostrar error de validacion
    const mostrarError = () => {
        dispatch({
            type: MOSTRAR_ERROR_NUEVA_CATEGORIA
        })
    }

    // Selecciona la categoria que el usuario dió clic
    const categoriaActual = id => {
        dispatch({
            type: CATEGORIA_ACTUAL,
            payload: id
        })
    }

    // Eliminar una categoría
    const eliminarCategoria = id => {
        dispatch({
            type: ELIMINAR_CATEGORIA,
            payload: id
        })
    }

    return(
        <categoriaContext.Provider
            value={{
                formcategoria: state.formcategoria,
                errorformulario: state.errorformulario,
                categorias: state.categorias,
                categoria: state.categoria,
                mostrarFormulario,
                obtenerCategorias,
                agregarCategoria,
                mostrarError,
                categoriaActual,
                eliminarCategoria,
            }}
        >
            { props.children }
        </categoriaContext.Provider>
    );
}

export default CategoriaState;