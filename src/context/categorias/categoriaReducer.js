import { 
    FORMULARIO_CATEGORIA,
    OBTENER_CATEGORIAS,
    AGREGAR_CATEGORIA,
    MOSTRAR_ERROR_NUEVA_CATEGORIA,
    CATEGORIA_ACTUAL,
    ELIMINAR_CATEGORIA,
} from '../../actiontypes';

export default (state, action) => {
    switch (action.type) {
        case FORMULARIO_CATEGORIA:
            return {
                ...state,
                formcategoria: true
            }
        case OBTENER_CATEGORIAS:
            return {
                ...state,
                categorias: action.payload
            }
        case AGREGAR_CATEGORIA:
            return {
                ...state,
                categorias: [ ...state.categorias, action.payload ],
                formcategoria: false,
                errorformulario: false
            }
        case MOSTRAR_ERROR_NUEVA_CATEGORIA:
            return {
                ...state,
                errorformulario: true
            }
        case CATEGORIA_ACTUAL:
            return {
                ...state,
                categoria: state.categorias.find(cat => cat.id === action.payload)
            }
        case ELIMINAR_CATEGORIA:
            return {
                ...state,
                categorias: state.categorias.filter(cat => cat.id !== action.payload),
                categoria: null
            }
        default:
            return state
    }
}