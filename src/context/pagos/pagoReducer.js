import { 
    PAGOS_CATEGORIA,
    AGREGAR_PAGO,
    MOSTRAR_ERROR_NUEVO_PAGO,
    ELIMINAR_PAGO,
    CAMBIAR_ESTADO_PAGO,
    PAGO_ACTUAL,
    MODIFICAR_PAGO,
} from '../../actiontypes';

export default (state, action) => {
    switch (action.type) {
        case PAGOS_CATEGORIA:
            return {
                ...state,
                pagoscategoria: state.pagos.filter(p => p.categoria_id === action.payload)
            }
        case AGREGAR_PAGO:
            return {
                ...state,
                pagos: [ ...state.pagos, action.payload ],
                errorformnuevopago: false
            }
        case MOSTRAR_ERROR_NUEVO_PAGO:
            return {
                ...state,
                errorformnuevopago: true
            }
        case ELIMINAR_PAGO:
            return {
                ...state,
                pagos: state.pagos.filter(p => p.id !== action.payload)
            }
        case CAMBIAR_ESTADO_PAGO:
            return {
                ...state,
                pagos: state.pagos.map(pago => (
                    pago.id === action.payload.id ? action.payload : pago
                ))
            }
        case PAGO_ACTUAL:
            return {
                ...state,
                pagoseleccionado: action.payload
            }
        case MODIFICAR_PAGO:
            return {
                ...state,
                pagos: state.pagos.map(pago => (
                    pago.id === action.payload.id ? action.payload : pago
                )),
                pagoseleccionado: null
            }
        default:
            return state
    }
}