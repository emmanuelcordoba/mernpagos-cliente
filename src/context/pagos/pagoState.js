import React, { useReducer } from 'react';
import pagoContext from './pagoContext';
import pagoReducer from './pagoReducer';
import {
    PAGOS_CATEGORIA,
    AGREGAR_PAGO,
    MOSTRAR_ERROR_NUEVO_PAGO,
    ELIMINAR_PAGO,
    CAMBIAR_ESTADO_PAGO,
    PAGO_ACTUAL,
    MODIFICAR_PAGO,
} from '../../actiontypes';
import { v4 as uuidv4 } from 'uuid';

const PagoState = props => {

    const initialState = {
        pagos: [
            { id:1, titulo:'Marzo 2020', monto:7100, pagado:true, categoria_id: 1 },
            { id:2, titulo:'Abril 2020', monto:7100, pagado:true, categoria_id: 1  },
            { id:3, titulo:'Mayo 2020', monto:7100, pagado:false, categoria_id: 1  },
            { id:4, titulo:'3 Abril 2020', monto:1100, pagado:true, categoria_id: 2 },
            { id:5, titulo:'17 Abril 2020', monto:1400, pagado:true, categoria_id: 2  },
            { id:6, titulo:'1-2020', monto:2100, pagado:true, categoria_id: 3  },
            { id:7, titulo:'2-2020', monto:2100, pagado:true, categoria_id: 3 },
            { id:8, titulo:'3-2020', monto:2400, pagado:false, categoria_id: 3 },
            { id:9, titulo:'Abril 2020', monto:500, pagado:true, categoria_id: 4  },
            { id:10, titulo:'Mayo 2020', monto:500, pagado:false, categoria_id: 4  },    
        ],
        pagoscategoria: null,
        errorformnuevopago: false,
        pagoseleccionado: null
    };

    // Crear state y dispatch
    const [state, dispatch] = useReducer(pagoReducer, initialState);

    // Funciones del CRUD

    // Obtener los pagos de una categoría
    const obtenerPagos = categoria_id => {
        dispatch({
            type: PAGOS_CATEGORIA,
            payload: categoria_id
        })
    }

    // Agrega un paco a la categoria seleccionada
    const agregarPago = pago => {
        pago.id = uuidv4();
        pago.pagado = false;

        dispatch({
            type: AGREGAR_PAGO,
            payload: pago
        });
    }

    // Muestra error de validación
    const mostrarErrorNuevoPago = () => {
        dispatch({
            type: MOSTRAR_ERROR_NUEVO_PAGO
        })
    }

    // Eliminar pago
    const eliminarPago = id => {
        dispatch({
            type: ELIMINAR_PAGO,
            payload: id
        });
    }

    // Cambia el estado de un Pago
    const cambiarEstadoPago = pago => {
        pago.pagado = !pago.pagado;
        dispatch({
            type: CAMBIAR_ESTADO_PAGO,
            payload: pago
        })
    }

    // Extrae un pago para editarlo
    const guardarPagoSeleccionado = pago => {
        dispatch({
            type: PAGO_ACTUAL,
            payload: pago
        })
    }

    // Modifica un pago
    const modificarPago = pago => {
        dispatch({
            type: MODIFICAR_PAGO,
            payload: pago
        })
    }


    return(
        <pagoContext.Provider
            value={{
                pagos: state.pagos,
                pagoscategoria: state.pagoscategoria,
                errorformnuevopago: state.errorformnuevopago,
                pagoseleccionado: state.pagoseleccionado,
                obtenerPagos,
                agregarPago,
                mostrarErrorNuevoPago,
                eliminarPago,
                cambiarEstadoPago,
                guardarPagoSeleccionado,
                modificarPago,
            }}
        >
            { props.children }
        </pagoContext.Provider>

    );
}

export default PagoState