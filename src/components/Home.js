import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Aside from './layout/Aside';
import Header from './layout/Header';
import NuevoPago from './pagos/NuevoPago';
import ListadoPagos from './pagos/ListadoPagos';

export default function Home() {
    return (
        <Container className="px-0" fluid>
            <Row className="mx-0">
                <Col xs={12} md={3} className="bg-light border-right">
                    <Aside />
                </Col>
                <Col xs={12} md={9} className="px-0">
                    <Header />
                    <main>
                        <Container>
                            <Row>
                                <NuevoPago />
                            </Row>
                            <Row>
                                <ListadoPagos />
                            </Row>
                        </Container>
                    </main>
                </Col>
            </Row> 
        </Container>
    )
}
