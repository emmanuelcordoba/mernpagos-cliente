import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';

export default function Header() {
    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand>
                <Navbar.Text className="mr-1">Hola</Navbar.Text>
                Emmanuel Córdoba
            </Navbar.Brand>
            <Nav className="ml-auto">
                <Nav.Link href="#!">Cerrar Sesión</Nav.Link>
            </Nav>
        </Navbar>
    )
}
