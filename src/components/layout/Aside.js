import React from 'react';
import NuevaCategoria from '../categorias/NuevaCategoria';
import ListadoCategorias from '../categorias/ListadoCategorias';

export default function Aside() {
    return (
        <aside className="p-2 h-100">
            <h3 className="text-center mb-3">MERN Pagos</h3>
            <NuevaCategoria />
            <h5 className="text-center">Tus Categorías</h5>
            <ListadoCategorias />
        </aside>
    )
}
