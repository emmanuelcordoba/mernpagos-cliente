import React, { useContext, useState, useEffect } from 'react';
import { Card, Form, Button, Row, Col, Alert } from 'react-bootstrap';
import categoriaContext from '../../context/categorias/categoriaContext';
import pagoContext from '../../context/pagos/pagoContext';

export default function NuevoPago() {

    const [pago, setPago] = useState({
        titulo: '',
        monto: ''
    });

    // Extraemos del pago
    const { titulo, monto } = pago;

    // Obtener el context de Categoría
    const categoriasContext = useContext(categoriaContext);
    const { categoria } = categoriasContext;

    // Obtener el context de Pago
    const pagosContext = useContext(pagoContext);
    const { 
        errorformnuevopago,
        pagoseleccionado,
        agregarPago, 
        obtenerPagos, 
        mostrarErrorNuevoPago,
        modificarPago
    } = pagosContext;

    useEffect(() => {
        if(pagoseleccionado){
            setPago(pagoseleccionado);
        } else {
            setPago({
                titulo: '',
                monto: ''
            });
        }
    }, [pagoseleccionado])

    // Si no hay categoria seleccionada
    if(!categoria) return null;

    // Cuando cambia el pago
    const onChangePago = e => {
        setPago({
            ...pago,
            [e.target.name]: e.target.value
        })
    }

    // Cuando se crea un pago
    const onSubmitNuevoPago = e => {
        e.preventDefault()

        // Valicación
        if(titulo.trim() === '' || monto.toString().trim() === ''){
            mostrarErrorNuevoPago();
            return;
        }

        if(!pagoseleccionado){
            // Alta de Pago
            // Agregar el pago al state
            pago.categoria_id = categoria.id;
            agregarPago(pago);
        } else {
            // Modificacion de Pago
            modificarPago(pago);
        }

        // Obtener pago de la categoria seleccionada
        obtenerPagos(categoria.id);

        // Reinciar el formulario
        setPago({
            titulo: '',
            monto: ''
        })
    }


    return (
        <Card bg="light" className="mx-auto mt-3">
            <Card.Body>
                <Form onSubmit={onSubmitNuevoPago}>
                    <Form.Group as={Row} controlId="formNuevoPagoTitulo">
                        <Form.Label
                            column
                            xs={3}
                            className="pr-0"
                        >Título</Form.Label>
                        <Col xs={9}>
                            <Form.Control
                                type="text"
                                name="titulo"
                                placeholder="Ingrese el título"
                                onChange={onChangePago}
                                value={titulo}
                            ></Form.Control>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formNuevoPagoTitulo">
                        <Form.Label
                            column
                            xs={3}
                            className="pr-0"
                        >Monto</Form.Label>
                        <Col xs={9}>
                            <Form.Control
                                type="number"
                                name="monto"
                                min="1"
                                step="0.01"
                                placeholder="Ingrese el monto"
                                onChange={onChangePago}
                                value={monto}
                            ></Form.Control>
                        </Col>
                    </Form.Group>
                    <Button
                        type="submit"
                        variant="primary"
                    >{ pagoseleccionado ? 'Modificar' : 'Agregar' }</Button>
                </Form>
                {
                    errorformnuevopago ? 
                    (
                        <Alert variant="danger" className="mt-2 text-center"
                        >El título y el monto son obligatorios
                        </Alert>
                    ) : null
                }
            </Card.Body>
        </Card>
    )
}
