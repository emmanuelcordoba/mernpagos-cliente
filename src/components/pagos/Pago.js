import React, { useContext } from 'react';
import { ListGroup, Row, Col, ButtonGroup, Button } from 'react-bootstrap';
import categoriaContext from '../../context/categorias/categoriaContext';
import pagoContext from '../../context/pagos/pagoContext';

export default function Pago({ pago }) {

    // Obtener el context de Categoría
    const categoriasContext = useContext(categoriaContext);
    const { categoria } = categoriasContext;
    
    // Obtener el context de Pago
    const pagosContext = useContext(pagoContext);
    const { 
        eliminarPago,
        obtenerPagos,
        cambiarEstadoPago,
        guardarPagoSeleccionado
    } = pagosContext;

    // Elimianr un pago
    const onClickEliminar = e => {
        if(window.confirm('¿Está seguro que desea eliminar el pago?')){
            eliminarPago(pago.id);
            obtenerPagos(categoria.id);
        }
    }

    return (
        <ListGroup.Item variant="secondary">
            <Row>
                <Col xs={2}>$ { pago.monto }</Col>
                <Col xs={6}>{ pago.titulo }</Col>
                <Col xs={4} className="text-right">
                    {
                        pago.pagado ? 
                        <Button 
                            variant="success" 
                            className="badge"
                            onClick={() => cambiarEstadoPago(pago)}
                        >Pagado</Button>
                        :
                        <Button 
                            variant="danger" 
                            className="badge"
                            onClick={() => cambiarEstadoPago(pago)}
                        >Impago</Button>
                    }
                    <ButtonGroup size="sm" className="ml-2">
                        <Button 
                            variant="secondary"
                            onClick={() => guardarPagoSeleccionado(pago)}
                        >Modifiar</Button>
                        <Button 
                            variant="dark"
                            onClick={onClickEliminar}
                        >Eliminar</Button>
                    </ButtonGroup>
                </Col>
            </Row>
        </ListGroup.Item>
    )
}
