import React, { Fragment, useContext }  from 'react'
import { ListGroup, Button } from 'react-bootstrap';
import Pago from './Pago';
import categoriaContext from '../../context/categorias/categoriaContext';
import pagoContext from '../../context/pagos/pagoContext';

export default function ListadoPagos() {
    
    // Obtener el context de Categoría
    const categoriasContext = useContext(categoriaContext);
    const { categoria, eliminarCategoria } = categoriasContext;

    // Obtener el context de Pago
    const pagosContext = useContext(pagoContext);
    const { pagoscategoria } = pagosContext;

    // Si no hay categoría seleccionada
    if(!categoria){
        return <h3 className="mt-3 mx-auto">Selecciona una categoría</h3>
    }

    // Cuando se elimina una categoría
    const onClickEliminarCategoria = e => {
        if(window.confirm('¿Está seguro que desea eliminar la categoría?')){
            eliminarCategoria(categoria.id);
        }
    }    

    
    return (
        <Fragment>
            <h4 className="w-100 mt-3 text-center">
                Categoría: { categoria.nombre }
            </h4>
            <ListGroup className="w-100 mx-5">
                {
                    pagoscategoria.map(pago =>(
                        <Pago
                            key={pago.id}
                            pago={pago}
                        />
                    ))
                }
            </ListGroup>
            <Button
                variant="outline-danger"
                className="mt-3 mx-auto"
                onClick={onClickEliminarCategoria}
            >Eliminar Categoría</Button>
        </Fragment>
    )
}
