import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

export default function Registro() {

    const [usuario, setUsuario] = useState({
        nombre: '',
        email: '',
        password: '',
        passwordconfirm: ''
    })

    // Extraemos del usuario
    const { nombre, email, password, passwordconfirm } = usuario;

    // Cuando el usuario cambia los inputs
    const onChangeUsuario = e => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    // Cuando el usuario envia el formulario
    const onSubmitRegistro = e => {
        e.preventDefault();
        
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col xs={12} sm={8} ms={6} className="mx-auto">
                    <Card bg="light">
                        <Card.Header>Crear Usuario</Card.Header>
                        <Card.Body>
                            <Form onSubmit={onSubmitRegistro}>
                                <Form.Group controlId="formRegistroNombre">
                                    <Form.Label>Nombre</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="nombre"
                                        placeholder="Ingresa tu nombre"
                                        onChange={onChangeUsuario}
                                        value={nombre}
                                    />
                                </Form.Group>
                                <Form.Group controlId="formRegistroEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control
                                        type="email"
                                        name="email"
                                        placeholder="Ingresa tu email"
                                        onChange={onChangeUsuario}
                                        value={email}
                                    />
                                </Form.Group>
                                <Form.Group controlId="formRegistroPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        name="password"
                                        placeholder="Ingresa tu password"
                                        onChange={onChangeUsuario}
                                        value={password}
                                    />
                                </Form.Group>
                                <Form.Group controlId="formRegistroPasswordconfirm">
                                    <Form.Label>Confirmar Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        name="passwordconfirm"
                                        placeholder="Ingresa tu password de nuevo"
                                        onChange={onChangeUsuario}
                                        value={passwordconfirm}
                                    />
                                </Form.Group>
                                <Button
                                    type="submit"
                                    variant="primary"
                                    className="mr-3"
                                >Crear usuario</Button>
                                <Link to="/registro">Iniciar sesión</Link>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
