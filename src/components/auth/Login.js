import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

export default function Login() {

    const [usuario, setUsuario] = useState({
        email: '',
        password: ''
    });

    // Extraemos del usuario
    const { email, password } = usuario;

    // Cuando el usuario cambia los inputs
    const onChangeUsuario = e => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        });
    };

    // Cuando el usuario inicie sesión
    const onSubmitLogin = e => {
        e.preventDefault();

    }

    return (
        <Container>
            <Row className="mt-5">
                <Col xs={12} sm={8} ms={6} className="mx-auto">
                    <Card bg="light">
                        <Card.Header>Login</Card.Header>
                        <Card.Body>
                            <Form onSubmit={onSubmitLogin}>
                                <Form.Group controlId="formLoginEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control
                                        type="email"
                                        name="email"
                                        placeholder="Ingresa tu email"
                                        onChange={onChangeUsuario}
                                        value={email}
                                    />
                                </Form.Group>
                                <Form.Group controlId="formLoginPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        name="password"
                                        placeholder="Ingresa tu password"
                                        onChange={onChangeUsuario}
                                        value={password}
                                    />
                                </Form.Group>
                                <Button
                                    type="submit"
                                    variant="primary"
                                    className="mr-3"
                                >Iniciar Sesión</Button>
                                <Link to="/registro">Crear usuario</Link>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
