import React, { useState, useContext } from 'react';
import { Form, Button, Alert } from 'react-bootstrap';
import categoriaContext from '../../context/categorias/categoriaContext';

export default function NuevaCategoria() {

    // Obtenemos del State
    const categoriasContext = useContext(categoriaContext);
    const { 
        formcategoria,
        errorformulario,
        mostrarFormulario, 
        agregarCategoria,
        mostrarError
    } = categoriasContext;
    
    const [categoria, setCategoria] = useState({
        nombre: ''
    });

    // Extraer de la categoría
    const { nombre } = categoria;

    // Cuando el usuario cambia el input
    const onChangeCategoria = e => {
        setCategoria({
            ...categoria,
            [e.target.name]: e.target.value
        })
    }

    // Cuando el usuario crea una categoría
    const onSubmitNuevaCategoria = e => {
        e.preventDefault();

        // Validacion 
        if(nombre.trim() === ''){
            mostrarError();
            return;
        }

        // Agregar al state
        agregarCategoria(categoria);

        // Limpiar el formulario
        setCategoria({
            nombre: ''
        })
    }

    return (
        <div className="mb-5">
            <Button 
                type="button"
                variant="primary"
                className="mb-2"
                block
                onClick={mostrarFormulario}
            >Nueva Categoría
            </Button>
            {
                formcategoria ?
                (
                    <Form onSubmit={onSubmitNuevaCategoria}>
                        <Form.Group controlId="formCategoriaNombre">
                            <Form.Label>Categoria</Form.Label>
                            <Form.Control 
                                type="text"
                                name="nombre"
                                placeholder="Nombre de la categoría"
                                onChange={onChangeCategoria}
                                value={nombre}
                            />
                        </Form.Group>
                        <Button 
                            type="submit"
                            variant="primary"
                            block>Agregar Categoría
                        </Button>
                    </Form>
                ) : null
            }
            {
                errorformulario ? 
                (
                    <Alert variant="danger" className="mt-2 text-center"
                    >El nombre es obligatorio</Alert>
                ) : null
            }
        </div>
        
    )
}
