import React, { useContext } from 'react';
import { ListGroup } from 'react-bootstrap';
import categoriaContext from '../../context/categorias/categoriaContext';
import pagoContext from '../../context/pagos/pagoContext';

export default function Categoria({ categoria }) {
    
    // Obtener el context de Categoría
    const categoriasContext = useContext(categoriaContext);
    const { categoriaActual } = categoriasContext;

    // Obtener el context de Pago
    const pagosContext = useContext(pagoContext);
    const { obtenerPagos } = pagosContext;

    // Se selecciona una categoría
    const onClickCategoria = () => {
        categoriaActual(categoria.id);
        obtenerPagos(categoria.id);
    }

    return (
        <ListGroup.Item
            as="button"
            className="list-group-item-action"
            onClick={onClickCategoria}
        >{ categoria.nombre }            
        </ListGroup.Item>
    )
}
