import React, { useContext, useEffect } from 'react';
import Categoria from './Categoria';
import { ListGroup } from 'react-bootstrap';
import categoriaContext from '../../context/categorias/categoriaContext';

export default function ListadoCategorias() {
    
    // Obtener el context de Categoría
    const categoriasContext = useContext(categoriaContext);
    const { categorias, obtenerCategorias } = categoriasContext;

    useEffect(() => {
        obtenerCategorias();
    }, [])

    // Controlar si no hay categorias
    if(categorias.length === 0) return null;
    
    return (
        <ListGroup className="list-group-flush">
            {
                categorias.map(categoria => (
                    <Categoria 
                        key={categoria.id}
                        categoria={categoria} 
                    />
                ))
            }
        </ListGroup>
    )
}
